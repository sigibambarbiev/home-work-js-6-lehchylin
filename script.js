// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.

//    Экранирование в языках програмирования это возможность использовать в тексте специальные символы " ' [ ] \ ^ $ . | ? * + ( ) как обычные символы, т.е. если используются одинарные кавычки '', то в тексте апостроф ' будет восприниматься как закрытие одинарных кавычек. Поэтому для правильного отображения строки апостроф надо экранировать, \'. Например, 'I'l be back' надо записать так: 'I\'l bee back'

// 2. Які засоби оголошення функцій ви знаєте?

//    Function Declaration, в формате function 'имя функции'(параметры, принимаемые функцмей) {действия, которые будут выполняться после вызова функции}
//    Function Expression, функциональное выражение: const x = function(параметры, принимаемые функцмей) {действия, которые будут выполняться после вызова функции}
//    Именованные функциональные выражения: const x = function 'имя функции'(параметры, принимаемые функцмей) {действия, которые будут выполняться после вызова функции}

// 3. Що таке hoisting, як він працює для змінних та функцій?

//    Hoisting - это механизм в JS при котором функции в коде подготавливаются к выполнению еще до начала исполнения кода. В результате, функцию можно вызвать даже перед строкой в которой она была создана. Переменные let, const могут быть вызваны только после строки в которой они были созданы.

function createNewUser() {
    const firstName = prompt("Введіть ім'я", "Vasya");
    const lastName = prompt("Введіть прізвище", "Pupkin");
    let birthday = prompt("Введіть дату народження", "06.12.1976");

    function getLogin() {
        return (firstName.trim()[0] + lastName.trim()).toLowerCase()
    }

    function getAge() {
        birthday = birthday.split('.');
        birthday = new Date(birthday[2], birthday[1] - 1, birthday[0]);
        return Math.floor((new Date() - birthday) / (1000 * 60 * 60 * 24 * 365.25));
    }

    function getPassword() {
        return firstName.trim()[0].toUpperCase() + lastName.trim().toLowerCase() + birthday.getFullYear();
    }

    return {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin: getLogin(),
        getAge: getAge(),
        getPassword: getPassword()
    }
}

const newUser = createNewUser();

console.log(newUser);
console.log(newUser.getAge);
console.log(newUser.getPassword);


